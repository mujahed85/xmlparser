﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace XMLParser
{
    public class Helpers
    {
        public TenancyContract Task1_1_GetFilledObjectFromXML(string strXMLPath)
        {
            string xml = File.ReadAllText(strXMLPath);

            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(xml);
            string json = JsonConvert.SerializeObject(xmldoc);
            var jObj = (JObject)JsonConvert.DeserializeObject(json);
            var docsToUse = new List<JToken>();
            foreach (var doc in jObj)
            {
                if (doc.Key != "?xml")
                {
                    docsToUse.Add(doc.Value);
                }
            }
            TenancyContract objTenancyContract = null;
            foreach (var item in docsToUse)
            {
                objTenancyContract = new TenancyContract();
                objTenancyContract.ContractCode = Convert.ToInt32(item["ContractCode"].ToString());
                objTenancyContract.ContractAmount = Convert.ToInt32(item["ContractAmount"].ToString());
                objTenancyContract.ContractStartDate = Convert.ToDateTime(item["ContractStartDate"].ToString());
                objTenancyContract.ContractEndDate = Convert.ToDateTime(item["ContractEndDate"].ToString());
                objTenancyContract.ContractStatus = Convert.ToString(item["ContractStatus"].ToString());
                objTenancyContract.SecurityDeposit = Convert.ToInt32(item["SecurityDeposit"].ToString());
                objTenancyContract.PaymentCount = Convert.ToInt32(item["PaymentCount"].ToString());

                #region Filling TenantInformations in TenancyContract
                var jObjTenantInformations = ((item.Value<JContainer>()).ElementAt(10).Values<JContainer>()).ElementAt(0)["TenantInfo"].Value<JContainer>();
                TenantInformations objTenantInformations = new TenantInformations();

                //objTenantInformations.ContractCode = objTenancyContract.ContractCode;
                objTenantInformations.TenantCode = Convert.ToInt32(jObjTenantInformations["TenantCode"].ToString());
                objTenantInformations.TenantName = jObjTenantInformations["TenantName"].ToString();
                objTenantInformations.TenantType = Convert.ToInt32(jObjTenantInformations["TenantType"].ToString());
                objTenantInformations.TradeLicenseNumber = Convert.ToInt32(jObjTenantInformations["TradeLicenseNumber"].ToString());
                objTenantInformations.TradeLicenseState = Convert.ToInt32(jObjTenantInformations["TradeLicenseState"].ToString());

                objTenancyContract.objTenantInformations = objTenantInformations;
                #endregion

                #region Filling PropertyDetails in TenancyContract
                List<PropertyDetails> lstPropertyDetails = new List<PropertyDetails>();
                var jObjPropertyDetails = ((item.Value<JContainer>()).ElementAt(9).Values<JContainer>()).ElementAt(0)["PropertyDetails"];
                foreach (var ee5 in jObjPropertyDetails)
                {
                    PropertyDetails objPropertyDetails = new PropertyDetails();
                    objPropertyDetails.PropertyCode = ee5["PropertyCode"].ToString();
                    objPropertyDetails.AnnualRent = Convert.ToInt32(ee5["AnnualRent"].ToString()); 
                    //
                    objPropertyDetails.BuiltArea = Convert.ToInt32(ee5["BuiltArea"].ToString());
                    objPropertyDetails.IsRented = Convert.ToBoolean(ee5["IsRented"].ToString());
                    lstPropertyDetails.Add(objPropertyDetails);
                }
                objTenancyContract.objTenantInformations.lstPropertyInformations = lstPropertyDetails;
                #endregion

                

            }
            return (objTenancyContract);
        }

        internal int SaveIntoDatabaseUsingEF(TenancyContract objTenancyContract)
        {
            int iResultStatus = 0;
            try
            {
                using (EntitiesContext objEntitiesContext = new EntitiesContext())
                {
                    bool blnFound = FindObjectContainsOrNot(objTenancyContract.objTenantInformations.TenantCode, objEntitiesContext);
                    if (!blnFound)
                    {
                        objEntitiesContext.objTenancyContract.Add(objTenancyContract);
                        iResultStatus = objEntitiesContext.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return (iResultStatus);
        }

        private bool FindObjectContainsOrNot(int TenantCode, EntitiesContext objEntitiesContext)
        {
            bool blnResult = false;
            int iContractCodeDB=objEntitiesContext.Database.SqlQuery<int>("Select count(*) from TblTenantInformations where TenantCode=" + TenantCode.ToString()).FirstOrDefault();
            blnResult = (iContractCodeDB > 0 ? true:false);
            return (blnResult);
        }

        public DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name, type);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        public void PdfWithDt(DataTable dt)
        {
            try
            {
                Document document = new Document();
                string appRootDir = new DirectoryInfo(Environment.CurrentDirectory).Parent.Parent.FullName;
                appRootDir = appRootDir + "\\PDFs\\" + "Task1.5_TableCreate.pdf";
                if (File.Exists(appRootDir))
                {
                    File.Delete(appRootDir);
                }
                using (PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(appRootDir, FileMode.Create)))
                {
                    document.Open();
                    iTextSharp.text.Font font5 = iTextSharp.text.FontFactory.GetFont(FontFactory.HELVETICA, 5);

                    PdfPTable table = new PdfPTable(dt.Columns.Count);
                    float[] widths = new float[] { 4f, 4f, 4f, 4f, 4f, 4f };

                    table.SetWidths(widths);

                    table.WidthPercentage = 100;
                    PdfPCell cell = new PdfPCell(new Phrase("Products"));

                    cell.Colspan = dt.Columns.Count;

                    foreach (DataColumn c in dt.Columns)
                    {
                        table.AddCell(new Phrase(c.ColumnName, font5));
                    }

                    foreach (DataRow r in dt.Rows)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            table.AddCell(new Phrase(r[0].ToString(), font5));
                            table.AddCell(new Phrase(r[1].ToString(), font5));
                            table.AddCell(new Phrase(r[2].ToString(), font5));
                            table.AddCell(new Phrase(r[3].ToString(), font5));
                            table.AddCell(new Phrase(r[4].ToString(), font5));
                            table.AddCell(new Phrase(r[5].ToString(), font5));
                        }
                    }
                    document.Add(table);
                    document.Close();
                    MessageBox.Show("PDF Generated");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception in Generating PDF" + ex.Message);
            }
        }
    }
}
