﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace XMLParser
{
    public partial class frmParse : Form
    {
        private DataTable dtOutputClass;
        private Helpers objHelpers ;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        public frmParse()
        {
            InitializeComponent();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            openFileDialog1.RestoreDirectory = true;
            openFileDialog1.DefaultExt = "xml";

            dtOutputClass = new DataTable();
            objHelpers = new Helpers();
        }

        private void btnLoadXML_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtXMLPath.Text = openFileDialog1.FileName;
            }
        }
        private void btnParseSave_Click(object sender, EventArgs e)
        {
            #region XML PATH Validation
            string strXMLPath = txtXMLPath.Text;
            if (strXMLPath == "" && !System.IO.File.Exists(strXMLPath))
            {
                MessageBox.Show("Enter Correct Path");
                return;
            }
            #endregion

            TenancyContract objTenancyContract = null;
            
            #region Task1.1 Read (the attached) XML file from the file system
            try
            { 
                
                objTenancyContract = objHelpers.Task1_1_GetFilledObjectFromXML(strXMLPath); 
            }
            catch (Exception ex)
            {
                MessageBox.Show("Parsing XML Exception: " + ex.Message);
            }
            #endregion

            #region Task1.2
            try
            {
                int iStatus = objHelpers.SaveIntoDatabaseUsingEF(objTenancyContract);
                if (iStatus > 0)
                {
                    MessageBox.Show(iStatus+" Records Saved");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Saving XML Instance into Database Using EF Exception: " + ex.Message);
            }
            #endregion
        }
         
        private void btnPDF_Click(object sender, EventArgs e)
        {
            try
            {
                objHelpers.PdfWithDt(dtOutputClass);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception " + ex.Message);
            }
        }

        private void btnLoadData_Click(object sender, EventArgs e)
        {
            #region Calling SP From EF 
            try
            { 
                EntitiesContext objEntitiesContext = new EntitiesContext();
                string strSQL = "select COUNT(*) as Found from sys.procedures where name like '%sp_GetAllData%'";
                var SpFoundParam = new SqlParameter("Found", SqlDbType.Int)
                {
                    Direction = System.Data.ParameterDirection.Output
                };
                var data = objEntitiesContext.Database.ExecuteSqlCommand(strSQL, SpFoundParam);
                if (data == -1)//Sp not Found then create sp
                {

                }
                List<OutputClass> lstOutputClass = new List<OutputClass>();
                lstOutputClass = objEntitiesContext.Database.SqlQuery<OutputClass>("sp_GetAllData").ToList<OutputClass>();
                dtOutputClass = objHelpers.ToDataTable(lstOutputClass);

                dgvTendarContact.DataSource = dtOutputClass;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Loading into Grid Exception: " + ex.Message);
            }
            #endregion 
        }
    }
}
