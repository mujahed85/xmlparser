﻿namespace XMLParser
{
    internal class OutputClass
    {
        public int ContractCode { get; set; }
        public string PropertyCode { get; set; }
        public int AnnualRent { get; set; }
        public int BuiltArea { get; set; }
        public bool IsRented { get; set; }
        public string TenantName { get; set; }
    }
}