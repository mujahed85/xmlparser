﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XMLParser
{
    [Table("TblTenantInformations")]
    public class TenantInformations
    {
        [Key] 
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TenantCode { get; set; }
        [StringLength(50)]
        public string TenantName { get; set; }
        public int TenantType { get; set; }
        public int TradeLicenseNumber { get; set; }
        public int TradeLicenseState { get; set; }
        //public int ContractCode { get; set; }
        public List<PropertyDetails> lstPropertyInformations { get; set; }
        public TenantInformations()
        {
            lstPropertyInformations = new List<PropertyDetails>();
        }
    }
}
