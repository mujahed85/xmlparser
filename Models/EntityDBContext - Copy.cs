﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace XMLParser
{
    public class EntitiesContext : DbContext
    {
        public EntitiesContext() : base("name=CS_SQLServer")
        {

        }
        public DbSet<PropertyDetails> objPropertyDetails { get; set; }
        public DbSet<TenantInformations> TenantInformations { get; set; }
        public DbSet<TenancyContract> objTenancyContract { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Uses variables instead of constant strings
            string strTableName = "PropertyDetails", strSchemaName="dbo",strOprtnINSERT= "Insert", strOprtnUPDATE= "Update", strOprtnDELETE= "Delete";
            modelBuilder.Entity<PropertyDetails>()
            .MapToStoredProcedures(s => s.Insert(u => u.HasName(strOprtnINSERT + strTableName, strSchemaName))
                                            .Update(u => u.HasName(strOprtnUPDATE + strTableName, strSchemaName))
                                            .Delete(u => u.HasName(strOprtnDELETE + strTableName, strSchemaName))
            );

            strTableName = "TenantInformations";
            modelBuilder.Entity<TenantInformations>()
            .MapToStoredProcedures(s => s.Insert(u => u.HasName(strOprtnINSERT + strTableName, strSchemaName))
                                            .Update(u => u.HasName(strOprtnUPDATE + strTableName, strSchemaName))
                                            .Delete(u => u.HasName(strOprtnDELETE + strTableName, strSchemaName))
            );

            strTableName = "TenancyContract";
            modelBuilder.Entity<TenancyContract>()
            .MapToStoredProcedures(s => s.Insert(u => u.HasName(strOprtnINSERT + strTableName, strSchemaName))
                                            .Update(u => u.HasName(strOprtnUPDATE + strTableName, strSchemaName))
                                            .Delete(u => u.HasName(strOprtnDELETE + strTableName, strSchemaName))
            );
        } 
    }
}
