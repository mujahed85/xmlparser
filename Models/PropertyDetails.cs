﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace XMLParser
{
    [XmlRoot("TblPropertyDetails")]
    public class PropertyDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]

        [XmlElement("PropertyCode")]
        public string PropertyCode { get; set; }

        [XmlElement("AnnualRent")]
        public int AnnualRent { get; set; }

        [XmlElement("BuiltArea")]
        public int BuiltArea { get; set; }

        [XmlElement("IsRented")]
        public bool IsRented { get; set; }
        //[XmlElement]
        //public int Contara { get; set; }
    }
}
