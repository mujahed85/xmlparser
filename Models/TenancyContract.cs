﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace XMLParser
{
    //[XmlRoot("row")]
    [XmlRoot(Namespace = "http://www.w3.org/2001/XMLSchema-instance", ElementName = "TblTenancyContract", DataType = "string", 
        IsNullable = true)]
    [JsonObject(MemberSerialization.OptIn)]
    public class TenancyContract
    {
        #region POCO Properties Decalaration
        [XmlElement("ContractCode")]
        [JsonProperty]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ContractCode { get; set; }

        [XmlElement( "ContractAmount")]  
        public int ContractAmount { get; set; }

        [XmlElement("ContractStartDate")]
        public DateTime ContractStartDate { get; set; }

        [XmlElement("ContractEndDate")]
        public DateTime ContractEndDate { get; set; }

        [XmlElement("ContractStatus")]
        public string ContractStatus { get; set; }

        [XmlElement("SecurityDeposit")] 
        public int SecurityDeposit { get; set; }

        [XmlElement("PaymentCount")] 
        public int PaymentCount { get; set; }
        #endregion 

        [XmlElement("objTenantInformations_ContractCode")]
        public TenantInformations objTenantInformations { get; set; }

        public TenancyContract()
        {
            objTenantInformations = new TenantInformations();
        }
    }
}
