﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace XMLParser
{
    public class EntitiesContext : DbContext
    {
        public EntitiesContext() : base("name=CS_SQLServer")
        {

        }
        public DbSet<PropertyDetails> objPropertyDetails { get; set; }
        public DbSet<TenancyContract> objTenancyContract { get; set; }
        public DbSet<TenantInformations> TenantInformations { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<PropertyDetails>().MapToStoredProcedures();
            modelBuilder.Entity<TenancyContract>().MapToStoredProcedures();
            modelBuilder.Entity<TenantInformations>().MapToStoredProcedures();
        }

        //public override void Up(DbModelBuilder modelBuilder)
        //{
        //    modelBuilder.CreateStoredProcedure(
        //      "sp_GetAllData",
        //      p => new
        //      {
        //          id = p.Int()
        //      },
        //      @"SELECT TenancyContracts.ContractCode, PropertyDetails.PropertyCode, PropertyDetails.AnnualRent, PropertyDetails.BuiltArea, PropertyDetails.IsRented, TblTenantInformations.TenantName FROM PropertyDetails INNER JOIN TenancyContracts ON PropertyDetails.TenancyContract_ContractCode = TenancyContracts.ContractCode INNER JOIN TblTenantInformations ON TenancyContracts.objTenantInformations_TenantCode = TblTenantInformations.TenantCode"
        //    );
        //}

        //public override void Down()
        //{
        //    DropStoredProcedure("sp_GetAllData");
        //}

    }
}
