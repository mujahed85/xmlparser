namespace XMLParser.EntitiesMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PropertyDetails",
                c => new
                    {
                        PropertyCode = c.String(nullable: false, maxLength: 128),
                        AnnualRent = c.Int(nullable: false),
                        BuiltArea = c.Int(nullable: false),
                        IsRented = c.Boolean(nullable: false),
                        TenantInformations_TenantCode = c.Int(),
                    })
                .PrimaryKey(t => t.PropertyCode)
                .ForeignKey("dbo.TblTenantInformations", t => t.TenantInformations_TenantCode)
                .Index(t => t.TenantInformations_TenantCode);
            
            CreateTable(
                "dbo.TenancyContracts",
                c => new
                    {
                        ContractCode = c.Int(nullable: false),
                        ContractAmount = c.Int(nullable: false),
                        ContractStartDate = c.DateTime(nullable: false),
                        ContractEndDate = c.DateTime(nullable: false),
                        ContractStatus = c.String(),
                        SecurityDeposit = c.Int(nullable: false),
                        PaymentCount = c.Int(nullable: false),
                        objTenantInformations_TenantCode = c.Int(),
                    })
                .PrimaryKey(t => t.ContractCode)
                .ForeignKey("dbo.TblTenantInformations", t => t.objTenantInformations_TenantCode)
                .Index(t => t.objTenantInformations_TenantCode);
            
            CreateTable(
                "dbo.TblTenantInformations",
                c => new
                    {
                        TenantCode = c.Int(nullable: false),
                        TenantName = c.String(maxLength: 50),
                        TenantType = c.Int(nullable: false),
                        TradeLicenseNumber = c.Int(nullable: false),
                        TradeLicenseState = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.TenantCode);
            
            CreateStoredProcedure(
                "dbo.PropertyDetails_Insert",
                p => new
                    {
                        PropertyCode = p.String(maxLength: 128),
                        AnnualRent = p.Int(),
                        BuiltArea = p.Int(),
                        IsRented = p.Boolean(),
                        TenantInformations_TenantCode = p.Int(),
                    },
                body:
                    @"INSERT [dbo].[PropertyDetails]([PropertyCode], [AnnualRent], [BuiltArea], [IsRented], [TenantInformations_TenantCode])
                      VALUES (@PropertyCode, @AnnualRent, @BuiltArea, @IsRented, @TenantInformations_TenantCode)"
            );
            
            CreateStoredProcedure(
                "dbo.PropertyDetails_Update",
                p => new
                    {
                        PropertyCode = p.String(maxLength: 128),
                        AnnualRent = p.Int(),
                        BuiltArea = p.Int(),
                        IsRented = p.Boolean(),
                        TenantInformations_TenantCode = p.Int(),
                    },
                body:
                    @"UPDATE [dbo].[PropertyDetails]
                      SET [AnnualRent] = @AnnualRent, [BuiltArea] = @BuiltArea, [IsRented] = @IsRented, [TenantInformations_TenantCode] = @TenantInformations_TenantCode
                      WHERE ([PropertyCode] = @PropertyCode)"
            );
            
            CreateStoredProcedure(
                "dbo.PropertyDetails_Delete",
                p => new
                    {
                        PropertyCode = p.String(maxLength: 128),
                        TenantInformations_TenantCode = p.Int(),
                    },
                body:
                    @"DELETE [dbo].[PropertyDetails]
                      WHERE (([PropertyCode] = @PropertyCode) AND (([TenantInformations_TenantCode] = @TenantInformations_TenantCode) OR ([TenantInformations_TenantCode] IS NULL AND @TenantInformations_TenantCode IS NULL)))"
            );
            
            CreateStoredProcedure(
                "dbo.TenancyContract_Insert",
                p => new
                    {
                        ContractCode = p.Int(),
                        ContractAmount = p.Int(),
                        ContractStartDate = p.DateTime(),
                        ContractEndDate = p.DateTime(),
                        ContractStatus = p.String(),
                        SecurityDeposit = p.Int(),
                        PaymentCount = p.Int(),
                        objTenantInformations_TenantCode = p.Int(),
                    },
                body:
                    @"INSERT [dbo].[TenancyContracts]([ContractCode], [ContractAmount], [ContractStartDate], [ContractEndDate], [ContractStatus], [SecurityDeposit], [PaymentCount], [objTenantInformations_TenantCode])
                      VALUES (@ContractCode, @ContractAmount, @ContractStartDate, @ContractEndDate, @ContractStatus, @SecurityDeposit, @PaymentCount, @objTenantInformations_TenantCode)"
            );
            
            CreateStoredProcedure(
                "dbo.TenancyContract_Update",
                p => new
                    {
                        ContractCode = p.Int(),
                        ContractAmount = p.Int(),
                        ContractStartDate = p.DateTime(),
                        ContractEndDate = p.DateTime(),
                        ContractStatus = p.String(),
                        SecurityDeposit = p.Int(),
                        PaymentCount = p.Int(),
                        objTenantInformations_TenantCode = p.Int(),
                    },
                body:
                    @"UPDATE [dbo].[TenancyContracts]
                      SET [ContractAmount] = @ContractAmount, [ContractStartDate] = @ContractStartDate, [ContractEndDate] = @ContractEndDate, [ContractStatus] = @ContractStatus, [SecurityDeposit] = @SecurityDeposit, [PaymentCount] = @PaymentCount, [objTenantInformations_TenantCode] = @objTenantInformations_TenantCode
                      WHERE ([ContractCode] = @ContractCode)"
            );
            
            CreateStoredProcedure(
                "dbo.TenancyContract_Delete",
                p => new
                    {
                        ContractCode = p.Int(),
                        objTenantInformations_TenantCode = p.Int(),
                    },
                body:
                    @"DELETE [dbo].[TenancyContracts]
                      WHERE (([ContractCode] = @ContractCode) AND (([objTenantInformations_TenantCode] = @objTenantInformations_TenantCode) OR ([objTenantInformations_TenantCode] IS NULL AND @objTenantInformations_TenantCode IS NULL)))"
            );
            
            CreateStoredProcedure(
                "dbo.TenantInformations_Insert",
                p => new
                    {
                        TenantCode = p.Int(),
                        TenantName = p.String(maxLength: 50),
                        TenantType = p.Int(),
                        TradeLicenseNumber = p.Int(),
                        TradeLicenseState = p.Int(),
                    },
                body:
                    @"INSERT [dbo].[TblTenantInformations]([TenantCode], [TenantName], [TenantType], [TradeLicenseNumber], [TradeLicenseState])
                      VALUES (@TenantCode, @TenantName, @TenantType, @TradeLicenseNumber, @TradeLicenseState)"
            );
            
            CreateStoredProcedure(
                "dbo.TenantInformations_Update",
                p => new
                    {
                        TenantCode = p.Int(),
                        TenantName = p.String(maxLength: 50),
                        TenantType = p.Int(),
                        TradeLicenseNumber = p.Int(),
                        TradeLicenseState = p.Int(),
                    },
                body:
                    @"UPDATE [dbo].[TblTenantInformations]
                      SET [TenantName] = @TenantName, [TenantType] = @TenantType, [TradeLicenseNumber] = @TradeLicenseNumber, [TradeLicenseState] = @TradeLicenseState
                      WHERE ([TenantCode] = @TenantCode)"
            );
            
            CreateStoredProcedure(
                "dbo.TenantInformations_Delete",
                p => new
                    {
                        TenantCode = p.Int(),
                    },
                body:
                    @"DELETE [dbo].[TblTenantInformations]
                      WHERE ([TenantCode] = @TenantCode)"
            );
            CreateStoredProcedure(
                name: 
                    "dbo.sp_GetAllData",
                body:
                    @"SELECT TenancyContracts.ContractCode, PropertyDetails.PropertyCode, PropertyDetails.AnnualRent, PropertyDetails.BuiltArea, PropertyDetails.IsRented, TblTenantInformations.TenantName FROM PropertyDetails INNER JOIN TenancyContracts ON PropertyDetails.TenancyContract_ContractCode = TenancyContracts.ContractCode INNER JOIN TblTenantInformations ON TenancyContracts.objTenantInformations_TenantCode = TblTenantInformations.TenantCode"
            ); 
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.TenantInformations_Delete");
            DropStoredProcedure("dbo.TenantInformations_Update");
            DropStoredProcedure("dbo.TenantInformations_Insert");
            DropStoredProcedure("dbo.TenancyContract_Delete");
            DropStoredProcedure("dbo.TenancyContract_Update");
            DropStoredProcedure("dbo.TenancyContract_Insert");
            DropStoredProcedure("dbo.PropertyDetails_Delete");
            DropStoredProcedure("dbo.PropertyDetails_Update");
            DropStoredProcedure("dbo.PropertyDetails_Insert");
            DropStoredProcedure("dbo.PropertyDetails_Insert");
            DropStoredProcedure("dbo.sp_GetAllData");
            DropForeignKey("dbo.TenancyContracts", "objTenantInformations_TenantCode", "dbo.TblTenantInformations");
            DropForeignKey("dbo.PropertyDetails", "TenantInformations_TenantCode", "dbo.TblTenantInformations");
            DropIndex("dbo.TenancyContracts", new[] { "objTenantInformations_TenantCode" });
            DropIndex("dbo.PropertyDetails", new[] { "TenantInformations_TenantCode" });
            DropTable("dbo.TblTenantInformations");
            DropTable("dbo.TenancyContracts");
            DropTable("dbo.PropertyDetails");
        }
    }
}
