﻿namespace XMLParser
{
    partial class frmParse
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvTendarContact = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnLoadData = new System.Windows.Forms.Button();
            this.btnParseSave = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnLoadXML = new System.Windows.Forms.Button();
            this.txtXMLPath = new System.Windows.Forms.TextBox();
            this.lblXMLPath = new System.Windows.Forms.Label();
            this.btnPDF = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTendarContact)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvTendarContact
            // 
            this.dgvTendarContact.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTendarContact.Location = new System.Drawing.Point(24, 171);
            this.dgvTendarContact.Name = "dgvTendarContact";
            this.dgvTendarContact.Size = new System.Drawing.Size(697, 267);
            this.dgvTendarContact.TabIndex = 8;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnLoadData);
            this.groupBox2.Controls.Add(this.btnParseSave);
            this.groupBox2.Location = new System.Drawing.Point(24, 105);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(697, 60);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Task 1.2";
            // 
            // btnLoadData
            // 
            this.btnLoadData.Location = new System.Drawing.Point(288, 19);
            this.btnLoadData.Name = "btnLoadData";
            this.btnLoadData.Size = new System.Drawing.Size(166, 23);
            this.btnLoadData.TabIndex = 1;
            this.btnLoadData.Text = "3.Load Data to Grid";
            this.btnLoadData.UseVisualStyleBackColor = true;
            this.btnLoadData.Click += new System.EventHandler(this.btnLoadData_Click);
            // 
            // btnParseSave
            // 
            this.btnParseSave.Location = new System.Drawing.Point(116, 19);
            this.btnParseSave.Name = "btnParseSave";
            this.btnParseSave.Size = new System.Drawing.Size(166, 23);
            this.btnParseSave.TabIndex = 1;
            this.btnParseSave.Text = "2.Parse and Save";
            this.btnParseSave.UseVisualStyleBackColor = true;
            this.btnParseSave.Click += new System.EventHandler(this.btnParseSave_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnLoadXML);
            this.groupBox1.Controls.Add(this.txtXMLPath);
            this.groupBox1.Controls.Add(this.lblXMLPath);
            this.groupBox1.Location = new System.Drawing.Point(24, 26);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(697, 60);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Task 1.1";
            // 
            // btnLoadXML
            // 
            this.btnLoadXML.Location = new System.Drawing.Point(592, 19);
            this.btnLoadXML.Name = "btnLoadXML";
            this.btnLoadXML.Size = new System.Drawing.Size(99, 23);
            this.btnLoadXML.TabIndex = 1;
            this.btnLoadXML.Text = "1. Load XML";
            this.btnLoadXML.UseVisualStyleBackColor = true;
            this.btnLoadXML.Click += new System.EventHandler(this.btnLoadXML_Click);
            // 
            // txtXMLPath
            // 
            this.txtXMLPath.Location = new System.Drawing.Point(76, 19);
            this.txtXMLPath.Name = "txtXMLPath";
            this.txtXMLPath.Size = new System.Drawing.Size(510, 20);
            this.txtXMLPath.TabIndex = 2;
            this.txtXMLPath.Text = "D:\\Mujahed\\XMLParsers\\XMLParser\\XMLParser\\bin\\Debug\\TenancyContract.xml";
            // 
            // lblXMLPath
            // 
            this.lblXMLPath.AutoSize = true;
            this.lblXMLPath.Location = new System.Drawing.Point(16, 19);
            this.lblXMLPath.Name = "lblXMLPath";
            this.lblXMLPath.Size = new System.Drawing.Size(54, 13);
            this.lblXMLPath.TabIndex = 0;
            this.lblXMLPath.Text = "XML Path";
            // 
            // btnPDF
            // 
            this.btnPDF.Location = new System.Drawing.Point(291, 462);
            this.btnPDF.Name = "btnPDF";
            this.btnPDF.Size = new System.Drawing.Size(166, 23);
            this.btnPDF.TabIndex = 1;
            this.btnPDF.Text = "4.Generate and Open PDF";
            this.btnPDF.UseVisualStyleBackColor = true;
            this.btnPDF.Click += new System.EventHandler(this.btnPDF_Click);
            // 
            // frmParse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(751, 497);
            this.Controls.Add(this.btnPDF);
            this.Controls.Add(this.dgvTendarContact);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmParse";
            this.Text = "Parse XML";
            ((System.ComponentModel.ISupportInitialize)(this.dgvTendarContact)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvTendarContact;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnParseSave;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnLoadXML;
        private System.Windows.Forms.TextBox txtXMLPath;
        private System.Windows.Forms.Label lblXMLPath;
        private System.Windows.Forms.Button btnPDF;
        private System.Windows.Forms.Button btnLoadData;
    }
}

